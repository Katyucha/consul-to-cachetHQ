import json
import re
import base64
import requests
import configparser
from enum import Enum
from time import sleep

def getlistCachetService(url):
  # Liste des composants dans CachetHQ
  f = requests.get(url + '/api/v1/components')
  slf = f.json()

  serviceListe = []
  for foo in slf["data"]:
    myTags = foo["tags"]
    myIdComponent = foo["id"]
    mylistConsul= []
    for key, serviceTag in myTags.items():
      mylistConsul.append(serviceTag)
    serviceListe.append(Service(mylistConsul,myIdComponent))
  return serviceListe

def getlistConsulService(url):
  f = requests.get( url + '/v1/catalog/services')
  slf = f.json().keys()
  myList = []
  for key in slf:
    myList.append(key)
  servicesConsulList = [ s for s in myList ]
  return servicesConsulList

def getStateCachetService(cachetServiceId, TOKEN):
  headers = {"X-Cachet-Token": TOKEN, "Content-Type":"application/json" }
  urlGet = urlCachet + 'api/v1/components/' + str(cachetServiceId)
  f = requests.get( urlGet,  headers=headers)
  state = f.json()["data"]["status"]
  return Etat(int(state))

def mainFunction(TOKEN, urlCachet, urlConsul):

    servicesConsulList = getlistConsulService(urlConsul)
    serviceListe = getlistCachetService(urlCachet)

    regexOptional = re.compile(".*:O")

    ## Mise à jour des services CachetHQ s'ils existent dans consul (version current)
    for service in serviceListe:
      etatService = Etat.OK
      for consulService in service.listServiceTagsName:
        isOptional = True if regexOptional.match(consulService) else False
        consulService = consulService.replace(":O","")
        if consulService not in servicesConsulList:
          if isOptional and etatService != Etat.Major:
              etatService = Etat.Minor
          else:
            etatService = Etat.Major
      oldState = getStateCachetService(service.idComponent, TOKEN)
      if oldState != etatService:
          if etatService == Etat.OK:
            payload = {"status": "1"}
          elif etatService == Etat.Performance:
            payload = {"status": "2"}
          elif etatService == Etat.Minor:
            payload = {"status": "3"}
          elif etatService == Etat.Major:
            payload = {"status": "4"}

          headers = {"X-Cachet-Token": TOKEN, "Content-Type":"application/json" }
          url = urlCachet + "api/v1/components/" + str(service.idComponent)
          print(str(service.idComponent) + " " + str(payload))
          response = requests.put(url , data=json.dumps(payload), headers=headers)

if __name__ == '__main__':

  class Etat(Enum):
    OK = 1
    Performance = 2
    Minor = 3
    Major = 4

  class Service:
    def __init__(self, listServiceTagsName, idComponent):
      self.listServiceTagsName = listServiceTagsName
      self.idComponent = idComponent

  config = configparser.ConfigParser()
  config.read('config.cfg')
  if config['DEFAULT']:
    refresh = config['DEFAULT']['refresh']
    TOKEN = config['DEFAULT']['TOKEN']
    urlCachet = config['DEFAULT']['urlCachet']
    urlConsul = config['DEFAULT']['urlConsul']
  else:
    print("EXIT: erreur de configuration")
    exit(255)

  while True:
    mainFunction(TOKEN, urlCachet, urlConsul)
    sleep(int(refresh))

#EOF
